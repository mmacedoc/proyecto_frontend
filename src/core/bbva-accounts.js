import {PolymerElement, html} from '@polymer/polymer';
import '@polymer/app-layout/app-layout.js';
import '@polymer/iron-collapse/iron-collapse.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-menu-button/paper-menu-button.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-item/paper-item.js';

import './bbva-menu-item.js';
import './bbva-quiero.js';
import './bbva-milista.js';
import './bbva-montofrm-control.js';
import '../operaciones/bbvanuevacta-app.js'
import '../operaciones/bbvatrnpropias-app.js'
import '../operaciones/bbvatrnterceros-app.js';

class AccountsElement extends PolymerElement {
  static get template() {
    return html`
      <style>
          :host{
            /*background: white;*/
            display:block;
            margin: auto;
            width: 50%;
          }          
          .accounts-content {
            border-radius: 5px;
            
            width: 580px;
            margin-top: 60px;
          }

          .title-table{
            position: relative;
            background: #DBDEE1;
            width: 100%;
            height:50px;
            /*border-style: solid;
            /*border-width: 0.2px;*/
            border-radius: 2px;
          }
          .title-table-inside{
            left: 10px;
            position: absolute;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
          }

          .title-table-chevron{            
            position: absolute;
            right: 10px;
            top: 50%;
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
            cursor: pointer;
          }
          .tblaccount{
            margin:auto;
            padding:0;
            width: 100%;
            border-collapse: collapse;
          }
          .account-detail{
            display: inline-block;
            width:100%;

          }
          .account-detail-compon{
            display: inline-block;
            vertical-align: middle;
            margin-left: 10px;
          }          
          .btnquierodiv{
            margin-left:50px;
            display: inline-block;
            color: white;       
          }
          .btnquiero{           
            background-color: #004481;            
            height:30px;
            width: 60px;
            border-radius: 0px;
          }
          paper-listbox{
            margin-top: 10px;
          }
          .account-detail-saldo-col{
            position:relative;
            vertical-align: middle;
          }
          .account-detail-saldo{
            position: absolute;
            right: 10px;
            font-weight: bold;
          }
          .thaccount{
            background: #E3E7EA;
            height: 40px;
            border: 1px solid #ccc;
            text-align: center;
          }
          .traccount {      
            height: 70px;      
            border-bottom: 1px solid #ccc;
          }
          .traccount:hover{
            cursor:pointer;
            background: #E3E7EA;
          }

          .title-drawer{
            color: white;
            margin-top: 10px;
            margin-bottom: 10px;
          }
          .item-adicional{
            float: right;
            margin-right: 10px;
            margin-top: 20px;
          }
          .item-compon{
            display: inline-block;
            vertical-align: middle;
            margin-right: 10px;
            color: white;
          }

          .select-account{
            opacity: 0.65;
          }

          .saldototal{
            color: #6B89D0;
            display: block;
            padding-top: 20px;
            height: 50px;
            text-align: right;
            font-size: 1.1em;
          }
          .txtsaldo{
            font-size: 0.8em;
          }
          .btnbar{
            background-color: #004481;
            color: white;
            height:40px;
            margin: 10px;
         }
          
      </style>
            
            <div id="drawer" class="accounts-content">
            <div>
              <paper-button class="btnbar" on-click="registroCuenta">                          
                <span>Nueva Cuenta</span>
                <iron-icon icon="icons:open-in-new"></iron-icon>
             </paper-button>
             <registrocta-element registronuevo={{registronuevo}}></registrocta-element>
          </div>
            <div class="title-table">
              <div class="title-table-inside">
                  <div>Cuentas</div>
              </div>
              <div class="title-table-chevron">
                  <iron-icon on-click="colapsar"                   
                  icon="{{iconoflecha}}"
                  slot="item-icon"></iron-icon>
              </div>
            </div>

            <iron-collapse id="collapse" opened="[[abierto]]">
              <table class="tblaccount">
                <tr class="thaccount">
                    <td class="thaccount">Tipo y Nro de Cuenta</td>
                    <td class="thaccount" >Saldo Contable</td>
                    <td class="thaccount">Saldo Disponible</td>
                </tr>
                <template is="dom-repeat" items="{{accounts}}">
                    <tr class="traccount" on-click="seleccionar">
                      <td>
                        <div class="account-detail">
                          <div class="account-detail-compon">
                            <div><b>[[item.numero]][[item.identificador]]</b></div>                        
                            <div>[[item.nombre]]</div>                        
                          </div>
                          <div class="btnquierodiv">
                            <paper-menu-button vertical-align="top" on-click="nohacernada" vertical-offset="38"
                            horizontal-offset="13">
                            <paper-button slot="dropdown-trigger" raised class="btnquiero">                          
                              <span>Quiero</span>
                              <iron-icon icon="hardware:keyboard-arrow-down"></iron-icon>
                            </paper-button>
                              <paper-listbox slot="dropdown-content">
                                <paper-item on-click="trnPropias">Transferencia entre mis cuentas</paper-item>
                                <paper-item on-click="trnTerceros">A Cuentas de terceros</paper-item>
                                <paper-item>A cuentas de Otros Bancos</paper-item>
                              </paper-listbox>
                            </paper-menu-button>
                          </div>
                        </div>
                      </td>
                      <td class="account-detail-saldo-col"><div class="account-detail-saldo">
                          [[item.moneda]] <monto-format-element monto={{item.saldo}}></monto-format-element></div></td>
                      <td class="account-detail-saldo-col"><div class="account-detail-saldo">
                          [[item.moneda]] <monto-format-element monto={{item.saldo}}></monto-format-element></div></td>
                    </tr>
                </template>
              </table>              
            </iron-collapse>
            <div class="saldototal">
              <span class="txtsaldo">Saldo total Disponible</span>
              <span>S/.</span>
              <span><b><monto-format-element monto="{{sumatotal}}"></monto-format-element></b></span>
            </div>      
              <div class="title-drawer">Mis Cuentas</div>
              

              <div class="item-adicional">
                <div class="item-compon">Ocultar Saldos</div>
                <mwc-switch checked=[[verSaldo]] class="item-compon"></mwc-switch>
              </div>
            </div>

            <transpropias-element registronuevo={{registrotrpropias}} ctaorigen={{cuentaorigen}}></transpropias-element>
            <transtercerros-element registronuevo={{registrotrterceros}}></transtercerros-element>


      
    `;
  }
  static get properties() {
    return {
        verSaldo: {
            type: Boolean,
            value: true,
            observer: 'verSaldoChange'
        },        
        accounts: {
          type: Array
        },
        sumatotal:{
          type: Number
        },
        abierto: {
          type: Boolean,
          value: true
        },
        iconoflecha: {
          type: String,
          value: 'hardware:keyboard-arrow-down'
        },

        registronuevo: {
          type: Boolean,
          value: false
        },
        registrotrpropias: {
          type: Boolean,
          value: false
        },
        registrotrterceros:{
          type: Boolean,
          value: false
        },
        cuentaorigen:{
          type: Object
        }

    };
  }

  verSaldoChange(newValue, oldValue) {
    console.log(newValue);
  }

  ready() {
    super.ready();
    //this.addEventListener('conexionOk', this.conexionOk);
    //this.addEventListener('actualizarCuentas', this.obtenerCuentasAct);
  }
  colapsar(){
    this.abierto = !this.abierto;
    this.iconoflecha = this.abierto?'hardware:keyboard-arrow-down':'hardware:keyboard-arrow-left';
    
  }

  seleccionar(e){
    //console.log("enviando evento seleccionar cuenta");
    var account = e.model.item;
    this.dispatchEvent(new CustomEvent('seleccionarCuenta', {bubbles: true, composed: true,
      detail: account}));
  }

  nohacernada(e){
    e.stopPropagation();
  }

  registroCuenta(){
    this.registronuevo = true;
  }
  trnPropias(e){
    this.registrotrpropias = true;
  }

  trnTerceros(){
    this.registrotrterceros = true;
  }

}
customElements.define('accounts-element', AccountsElement);