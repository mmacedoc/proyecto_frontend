import {PolymerElement, html} from '@polymer/polymer';

import '@polymer/iron-icons/social-icons.js';
import '@polymer/iron-icons/maps-icons.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-button/paper-button.js';

class AccountDetailElement extends PolymerElement {
  static get template() {
    return html`
      <style>
          :host{
            /*background: white;*/
            display:block;
            margin: auto;
            width: 50%;
          }          
          .account-content {
            border-radius: 5px;            
            width: 680px;
            margin-top: 60px;
          }
          .dnrocuenta{
              margin-top: 40px;
          }
          .card-logo-izq{
            /*float: right;
            margin-right: 10px;*/
            position: absolute;
            right: .5em;
            top: 50%;
            transform: translate(0,-50%);
            display: inline-block;
            vertical-align: middle;
          }
          .card-detail{
              display: inline-block;
              width: 300px;
              height: 200px;
              vertical-align:top;
              margin-left: 20px;
          }

          .pagos{
              margin-top: 40px;
          }

          .prestamo-content{
              vertical-align:top;
              display: inline-block;
          }
          paper-button.accept {
            background-color: #0040ff;
            color: white;
            margin-top:35px;
            margin-left:0px;
            width:100%;
            --paper-button-raised-keyboard-focus: {
                background-color: #0000ff !important;
                color: white !important;
            };
          }
          paper-button.accept:hover {
            background-color: #0000ff !important;
            color: white !important;
          }
          .card-desc{
            margin-left: 10px;
            display: inline-block;
            width: 75%;
          }
          .movimientos{
            margin-top: 30px;
            margin-left: 20px;
            width: 70%;
          }
          .card-menu-compon{
            margin-left:15px;
            display: inline-block;
            vertical-align: middle;
            padding-top:0px;
            padding-bottom:0px;
          }
          .card-menu-chevron{
            position: absolute;
            right: .5em;
            top: 50%;
            transform: translate(0,-50%);
          }
          .card-menu{
             margin-top:10px;
             width:100%;
         }
         .mov-content{
             margin-top: -20px;
             padding-top: 0px;
         }
         .headercta{
             display: inline-block;
             position:relative;
             vertical-align: middle;
             width: 100%;
         }
         .leftheader{
            
         }
         .rightheader{
            position: absolute;
            right: .5em;
            top: 50%;
            transform: translate(0,-50%);
         }
         .baroperaciones{
             border-radius: 2px;
             background-color: #9FB7F1;
             height: 60px;
             margin-top: 25px;
         }
         .btnbar{
            background-color: #004481;
            color: white;
            height:40px;
            margin: 10px;
         }
         .ultmov{
             margin-top: 25px;
             margin-bottom: 25px;
         }
         .tbloperaciones{
            margin:auto;
            padding:0;
            width: 100%;
            border-collapse: collapse;
          }
         .thaccount{
            background: #E3E7EA;
            height: 40px;
            border: 1px solid #ccc;
            text-align: center;
          }
          .traccount {      
            height: 70px;      
            border-bottom: 1px solid #ccc;
          }
          .traccount:hover{
            cursor:pointer;
            background: #E3E7EA;
          }
          .colorrojo:{
              color: red;
          }

      </style>
      <div class="account-content">
          <div class="headercta">
            <div class="leftheader">
                <div>Número de cuenta:</div>
                <div>BBVA: [[account.numero]][[account.identificador]]</div>
                <div>CCI: 009-170-[[account.numero]][[account.identificador]]</div>
            </div>
            <div class=rightheader>
                <div>Disponible</div>
                <div><b>[[account.moneda]] [[saldoformateado]]</b></div>
                <div>Contable <b>[[account.moneda]] [[saldoformateado]]</b></div>
                <div>Consulta de Retenciones y cheques</div>
            </div>
          </div>
          <div class="baroperaciones">
            <div>
            <paper-button class="btnbar">                          
                <span>Búsquedas</span>
                <iron-icon icon="icons:search"></iron-icon>
            </paper-button>
            <paper-button class="btnbar">                          
                <span>Operaciones</span>
                <iron-icon icon="icons:credit-card"></iron-icon>
            </paper-button>
            </div>
            
          </div>
          <div class="ultmov">
              <span>Últimos movimientos</span>
          </div>
          <div>
          <table class="tbloperaciones">
                <tr class="thaccount">
                    <td class="thaccount">Fecha</td>
                    <td class="thaccount" >Descripción</td>
                    <td class="thaccount">ITF</td>
                    <td class="thaccount">Monto</td>  
                </tr>
                <template is="dom-repeat" items="{{transactions}}">
                    <tr class="traccount">
                      <td>
                        [[item.fecha]]
                      </td>
                      <td>
                        [[item.tipooperacion]]
                      </td>
                      <td>
                        [[item.moneda]] <monto-format-element monto=0></monto-format-element>
                      </td>
                      <td>                        
                          [[item.moneda]]<monto-format-element monto={{item.monto}}></monto-format-element>
                      </td>                      
                    </tr>
                </template>
              </table>              
          </div>
          
      </div>
    `;
  }
  static get properties() {
    return {
        account: {
            type: Object
        },
        saldoformateado: {
            type: String,
            computed: 'formatSaldo(account.saldo)'
        },
        transactions: {
            type: Array
        }
    };
  }

  formatSaldo(saldo) {
      var p = saldo.toFixed(2).split(".");
      return p[0].split("").reverse().reduce(function(acc, saldo, i, orig) {
        return  saldo=="-" ? acc : saldo + (i && !(i % 3) ? "," : "") + acc;
      }, "") + "." + p[1];
  }
}
customElements.define('accountdetail-element', AccountDetailElement);