import {PolymerElement, html} from '@polymer/polymer';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/social-icons.js';
import '@polymer/app-layout/app-layout.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/hardware-icons.js';


import './bbva-accounts.js'
import {Globals} from './globals.js'

class ToolBarElement extends PolymerElement {
  static get template() {
    return html`
      <style>
          app-header {
            display: block;
            background-color: #021F40;
            color: #000;
            /*border-top: 15px solid #0066ff;*/
            border-bottom: 1px solid #80aaff;
            height:80px;
          }
          app-header paper-icon-button {
            --paper-icon-button-ink-color: #fff;
          }
          
          paper-icon-button.btntoolbar {
            background-color: #021F40;
            color: white;
          }
          paper-icon-button.btntoolbar:hover {
            background-color: white;
            color: #021F40;
          }
          .txtuser{
            color: white;
          }
      </style>
      <app-header-layout >
        <app-header effects="waterfall" slot="header">
          
          <app-toolbar>
            <div main-title>
              <img src="https://www.bbva.pe/content/dam/public-web/global/images/logos/logo_bbva_blanco.svg" height="70"/>
            </div> 
            <div class="txtuser">
              [[usersession.persona.nombre]] [[usersession.persona.apellido]]
              [[Globals.semillaencriptado]]
            </div>                   
            <div class="navItem">
              <paper-icon-button icon="social:notifications" class="btntoolbar"
                aria-label="Notificaciones"></paper-icon-button>              
            </div>
            
            <paper-icon-button icon="social:person-outline" 
            class="btntoolbar"  aria-label="Datos de la persona"></paper-icon-button>
            <paper-icon-button icon="icons:highlight-off" on-click="cierrasession"
            class="btntoolbar" aria-label="Datos de la persona"></paper-icon-button>
          </app-toolbar>          
        </app-header>
        </app-header-layout>        
    `;
  }

  static get properties() {
    return {
      usersession: {
            type: Object
      }
    };
  }

  cargarIndex(){
    this.dispatchEvent(new CustomEvent('cargarindex', {bubbles: true, composed: true,
      detail:{}}));
  }

  cierrasession(){
    this.dispatchEvent(new CustomEvent('cierraSession', {bubbles: true, composed: true,
      detail: {}}));
  }
}
customElements.define('toolbar-element', ToolBarElement);