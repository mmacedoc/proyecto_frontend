import {PolymerElement, html} from '@polymer/polymer';

class MontoFormatElement extends PolymerElement {
  static get template() {
    return html`
        <style>
          .montorojo{
            color: red;
          }
          .montonegro{
            color: black;
          }
        </style>
        <span>[[saldoformateado]]</span>
    `;
  }
  static get properties() {
    return {
        monto: {
          type: Number
        },
        saldoformateado: {
          type: String,
          computed: 'formatSaldo(monto)'
        },
        verMonto: {
          type: Boolean,
          value: true
        },
        clasemonto: {
          type: String,
          value: "montonegro"
        }
    };
  }
  ready(){
    super.ready();
    //this.clasemonto = "montonegro";
    //console.log("monto: " + this.monto + " " + (this.monto<0))
    if(this.monto<0){
      this.clasemonto = "montorojo";
    }
    //console.log(this.clasemonto );
  }
  formatSaldo(monto) {
    var signo = "";
    if(monto<0){
      signo = "-";
    }
    if(this.verMonto){
      var p = monto.toFixed(2).split(".");
      return signo + "" + p[0].split("").reverse().reduce(function(acc, monto, i, orig) {
        return  monto=="-" ? acc : monto + (i && !(i % 3) ? "," : "") + acc;
      }, "") + "." + p[1];      
    }else{
      return signo + '******.**';
    }
  }

}
customElements.define('monto-format-element', MontoFormatElement);