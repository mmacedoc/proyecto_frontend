import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {Icon} from "@material/mwc-icon"
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-menu-button';

import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu-light.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-spinner/paper-spinner.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-dialog/paper-dialog.js';

//import 'bootstrap-elements/dist/bootstrap-elements.js';

import './bbvaregistro-app.js';
import './bbvalogin-navbar.js';

import {Globals} from '../core/globals.js'
/**
 * @customElement
 * @polymer
 */
class LoginElement extends PolymerElement {
  static get template() {
    return html`
      <style>
        .imgbkground {
          position: fixed; 
          top: 0; 
          left: 0;
                    
          /* Preserve aspet ratio */
          min-width: 100%;
          min-height: 50%;
        }

      </style>
      <login-navbar></login-navbar>
      <img src="src/images/fondo.png" class="imgbkground" alt="">




    `;
  }
  static get properties() {
    return {
      
    };
  }
/*
  registronuevo-changed(){
    console.log('cambio...... ' + this.registronuevo);
  }*/
  

}

window.customElements.define('login-element', LoginElement);
