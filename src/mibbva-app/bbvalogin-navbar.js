import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
/*import 'heavy-navbar/dist/heavynavbar.js';
import 'heavy-navbar/dist/heavynavbar/heavynavbar.esm.js';*/
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-menu-button';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/iron-icons/iron-icons.js';
import 'heavy-navbar/dist/heavynavbar.js';
import 'heavy-navbar/dist/heavynavbar/heavynavbar.esm.js';

import './bbvaregistro-app.js';
import {Globals} from '../core/globals.js'
/**
 * @customElement
 * @polymer
 */
class NavBarElement extends PolymerElement {
  static get template() {
    return html`
    <style>
        paper-dropdown-menu {
          width: 100% !important;
        }

        paper-button.accept {
          height: 50px;
          background-color: #0040ff;
          color: white;
          --paper-button-raised-keyboard-focus: {
            background-color: #0000ff !important;
            color: white !important;
          };
        }
        paper-button.accept:hover {
          background-color: #0000ff !important;
          color: white !important;
        }

        paper-button.green{
          background-color: #4BD813;
        }
        paper-button.green:hover{
          background-color: #47C714 !important;
        }
        .msj{
          color: red;
        }
        paper-dialog{
            width: 400px;
            margin: 0px;
            border-radius:  20px;
        }
      </style>



      <heavy-navbar  item-count="5" position="fixed">
          <a href="#" slot="item-1">Inicio</a>
          <a href="#" slot="item-2">Contactenos</a>
          
          <paper-button slot="item-3" toggles raised class="accept" on-click="abrelogin">
          <iron-icon icon="icons:perm-identity"></iron-icon>&nbsp;&nbsp;Acceder</paper-button>
          <paper-button slot="item-4" toggles raised class="accept green" on-click="abreregistro">
          <iron-icon icon="icons:thumb-up"></iron-icon>&nbsp;&nbsp;Registrarse</paper-button>
      </heavy-navbar>
      <paper-dialog id="dropdownDialog" modal opened=[[login]]>
        <h2>Datos de Acceso</h2>
        <div class="card-content">
        <paper-dropdown-menu label="Tipo de Documento" vertical-offset="60"
          value="{{tipodocumento}}" selected-item-changed="evalua">
          <paper-listbox slot="dropdown-content" class="dropdown-content"
            attr-for-selected="value" selected='L'>
            <paper-item value="L">DNI</paper-item>
            <paper-item value="R">RUC</paper-item>
            <paper-item value="P">Pasaporte</paper-item>
            <paper-item value="C">Carnet de Extranjeria</paper-item>
          </paper-listbox>
        </paper-dropdown-menu>
        

          <paper-input id="txtnrodocumento" label="Nro Documento" value="{{numerodocumento}}" required
            auto-validate error-message="Es obligatorio"></paper-input>
          <paper-input id="txtpassword" label="Password" type="password" value="{{contrasena}}" required
            auto-validate error-message="Es obligatorio"></paper-input>
        </div>
        <div align="center">
        <paper-spinner alt="Procesando" active="{{cargando}}"></paper-spinner>
        <span class="msj" active={{muestramensaje}}>{{mensaje}}</span>
        </div>
        <div class="buttons">            
              <paper-button autofocus on-click="accedelogin">
                <iron-icon icon="check"></iron-icon>Aceptar</paper-button>
              <paper-button autofocus on-click="cierralogin">
              <iron-icon icon="icons:close"></iron-icon>Cerrar</paper-button>
          </div>  
      </paper-dialog>

      <iron-ajax
        id="loginajax"
        url= [[urlreq]] 
        handle-as="json"
        loading="{{cargando}}"
        method="POST"
        content-type="application/json"
        on-response="loginCorrecto"
        on-error="loginInCorrecto"
        debounce-duration="500">
      </iron-ajax>

      <registrousuario-element registronuevo={{registronuevo}}></registrousuario-element>
    `;
  }
  static get properties() {
    return {
      login: {
        type: Boolean,
        value: false
      },
      registronuevo: {
        type: Boolean,
        value: false
      },
      cargando: {
        type: Boolean,
        value: false
      },
      urlreq: {
        type: String,
        value: Globals.urlbaseback + "usuario/login"
      },
      muestramensaje: {
        type: Boolean,
        value: false
      },
      mensaje:{
        type: String,
        value: ""
      },
      numerodocumento:{
        type: String,
        value: "45454555555"
      },
      contrasena:{
        type: String,
        value: "1234560"
      }
    };
  }

  abrelogin(){    
    this.login = true;
    this.cargando = false;
    this.muestramensaje = false;
    this.mensaje = "";
  }
  cierralogin(){
    this.login = false;
  }

  accedelogin() {
    if(this.numerodocumento == "" || 
      this.contrasena == ""){
        this.shadowRoot.getElementById('txtnrodocumento').validate();
        this.shadowRoot.getElementById('txtpassword').validate();
        return;
    }else{
      this.cargando = true;
      var jrequest = {
          "tipodocumento": "L",//this.tipodocumento,
          "numerodocumento": this.numerodocumento,
          "clave": this.contrasena
      };
      this.$.loginajax.body = jrequest;
      this.$.loginajax.generateRequest();
    }
  }

  loginCorrecto(event, request) {
    console.log(event.detail.response.mensaje);
    this.dispatchEvent(new CustomEvent('conexionOk', {bubbles: true, composed: true,
      detail: event.detail.response.mensaje}));
    this.cierralogin();
  }

  loginInCorrecto(event, error) {
    if(event.detail.request.xhr.response){
      this.mensaje = event.detail.request.xhr.response.mensaje;
    }else{
      this.mensaje = "Error Interno";
    }
    this.cargando = false;
    this.muestramensaje = true;
  }

  abreregistro(){
    this.registronuevo = true;
  }
}

window.customElements.define('login-navbar', NavBarElement);
