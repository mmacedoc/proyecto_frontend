import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import 'heavy-navbar/dist/heavynavbar.js';
import 'heavy-navbar/dist/heavynavbar/heavynavbar.esm.js';


class GraniteBootstrapExample extends PolymerElement {
    static get template() {
        return html`
            <heavy-navbar item-count="4" position="fixed">>
    <a href="#" slot="item-1">Home</a>
    <a href="#about" slot="item-2">About</a>
    <a href="#blog" slot="item-3">Blog</a>
    <a href="#contact" slot="item-4">Contact</a>
</heavy-navbar>`;
    }
    static get properties() {
        return {
            
        };
    }

    constructor() {
        super();
    }
}
window.customElements.define('granite-bootstrap-example', GraniteBootstrapExample);