import {PolymerElement, html} from '@polymer/polymer';

import '@polymer/paper-card/paper-card.js';
import '@polymer/iron-icons/notification-icons.js';
import '@polymer/paper-tabs/paper-tabs.js';
import '@polymer/paper-tabs/paper-tab.js';
import '@polymer/iron-pages/iron-pages.js';

import '../core/bbva-toolbar.js';
import '../core/bbva-accounts.js';
import '../core/bbva-page-index.js';
import '../core/bbva-offeronline.js';
import '../core/bbva-accountdetail.js';
import {Globals} from '../core/globals.js';

class IndexElement extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        .toolbar{
            /*position: fixed;*/
            left: 0;
            right: 0;
            top: 0;
            z-index:10;
        }
        paper-tabs{
          display: block;
          width: 100%;
          /*margin-top: 80px;*/
          height: 50px;
          background-color: #004481;
          --paper-tabs-selection-bar-color: #23BBD0;
          color: white;
        }
        iron-pages{          
          display: block;
          width: 100%;
        }
        /*
        .offers{
            position: fixed;
            bottom: 0;
            right: 0;
        }
        
        .drawer{
            z-index:1;
            position: fixed;
            display: inline-block;
            left: 0;
            top: 0;
            bottom: 0;            
        }
        .pageindex{
            display: inline-block;
            margin-left: 280px;
            margin-top: 80px;
        }
        accountdetail-element{
            display: inline-block;
            margin-left: 300px;
            margin-top: 80px;
        }*/
      </style>
        
        <toolbar-element class="toolbar" usersession=[[usersession]]></toolbar-element>
        <paper-tabs selected="{{seleccionado}}">
            <paper-tab on-click="reinicioInicio">Inicio</paper-tab>
            <paper-tab>Cuentas</paper-tab>
            <paper-tab>Tarjetas</paper-tab>
            <paper-tab>Préstamos</paper-tab>
            <paper-tab>Inversiones</paper-tab>
            <paper-tab>Seguros</paper-tab>
        </paper-tabs>

        <iron-pages selected="{{seleccionado}}">
          <div>
            <template is="dom-if" if="[[pageindex]]">
              <accounts-element class="drawer" accounts=[[accounts]] sumatotal=[[sumatotal]]></accounts-element>
            </template>
            <template is="dom-if" if="[[accountdetail]]">
              <accountdetail-element account=[[currentaccount]] transactions=[[transactions]]></accountdetail-element>
            </template>
          </div>
          <div>
            
          Page 2
          </div>
          <div>Page 3</div>
        </iron-pages>

        <!--
        <template is="dom-if" if="[[pageindex]]">
            <pageindex-element class="pageindex"></pageindex-element>
            <offeronline-element class="offers"></offeronline-element>
        </template>
        <template is="dom-if" if="[[accountdetail]]"> 
            <accountdetail-element account=[[currentaccount]] transactions=[[transactions]]></accountdetail-element>
        </template>-->
        
        <iron-ajax
            id="getAccounts"
            url= [[urlreq]] 
            handle-as="json"
            method="GET"
            on-response="renderAccounts"
            content-type="application/json"
            debounce-duration="500">
        </iron-ajax>

        <iron-ajax
            id="getTransactions"
            url= [[urltxs]] 
            handle-as="json"
            method="GET"
            on-response="renderTransactions"
            content-type="application/json"
            debounce-duration="500">
        </iron-ajax>
    `;
  }
  static get properties() {
    return {
        usersession: {
            type: Object
        },
        pageindex: {
            type: Boolean,
            value: true
        },
        accountdetail: {
            type: Boolean,
            value: false
        },
        accounts:{
            type: Array
        },
        urlreq: {
            type: String,
            value: Globals.urlbaseback + "account/"
        },
        currentaccount:{
            type: Object
        },
        urltxs: {
            type: String,
            value: Globals.urlbaseback + "transaction/"
        },
        transactions:{
            type: Array
        },
        seleccionado:{
          type: Number,
          value: 0
        }
    };
  }

  constructor(){
    super();    
  }

  ready() {
    super.ready();
    this.addEventListener('actualizarCuentas', this.obtenerCuentasAct);
    this.addEventListener('seleccionarCuenta', this.seleccionarCuenta);
    this.addEventListener('cargarindex', this.cargarindex);
    this.obtenerCuentas();    
  }

  renderAccounts(event, request) {
    
    this.accounts = JSON.parse(event.detail.response.mensaje);    
    this.sumatotal = 0;
    if(this.accounts){
      console.log(this.accounts);
      console.log(this.accounts.length);
      for(var i=0; i<this.accounts.length; i++){
        this.sumatotal += this.accounts[i].saldo;
      }
    }
  }

  obtenerCuentasAct(event){
    this.obtenerCuentas();
  }

  obtenerCuentas(){    
    if(sessionStorage.getItem("user-bbva-session")){     
      var usersession = JSON.parse(sessionStorage.getItem("user-bbva-session"));
      this.urlreq = Globals.urlbaseback + "account/"

      //if(!this.urlreq.endsWith(usersession._id.$oid)){
        this.urlreq = this.urlreq + usersession._id.$oid;
      //}      
      var headers = this.$.getAccounts.headers;
      headers.token = usersession.token;
      this.$.getAccounts.headers = headers;
      this.$.getAccounts.generateRequest();
    }
  }

  seleccionarCuenta(event){
      console.log("Enviando evento seleccionar cuenta");
      this.pageindex = false;
      this.accountdetail = true;
      this.currentaccount = event.detail;
      //console.log(this.currentaccount);
      this.obtenerMovimientos(this.currentaccount.identificador);
  }

  obtenerMovimientos(idaccount){    
    if(sessionStorage.getItem("user-bbva-session")){     
      var usersession = JSON.parse(sessionStorage.getItem("user-bbva-session"));
      if(!this.urltxs.endsWith(idaccount)){
        this.urltxs = Globals.urlbaseback + "transaction/" + idaccount;
      }      
      var headers = this.$.getTransactions.headers;
      headers.token = usersession.token;

      this.$.getTransactions.headers = headers;
      this.$.getTransactions.generateRequest();
    }
  }

  renderTransactions(event, request) {
      console.log(event.detail.response.mensaje);
    this.transactions = JSON.parse(event.detail.response.mensaje);
  }

  cargarindex(event){
    this.pageindex = true;
    this.accountdetail = false;
  }

  reinicioInicio(){
    this.pageindex = true;
    this.accountdetail = false;
    this.obtenerCuentas();
  }
}
customElements.define('index-element', IndexElement);