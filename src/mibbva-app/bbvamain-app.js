
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';

import './bbvalogin-app.js';
import './bbvaindex-app.js';



class BBVAMainApp extends PolymerElement {
  static get template() {
    return html`
    
      <style>
        :host {
          display: block;
          width: 100%;
        }
        
      </style>   
      


      <template is="dom-if" if="[[!authenticated]]">
        <login-element class="centrado" autenticado=[[authenticated]]></login-element>
      </template>
      <template is="dom-if" if="[[authenticated]]">
        <index-element usersession=[[usersession]]></index-element>
      </template>
      
       

    `;
  }

  
  static get properties() {
    return {
        authenticated: {
            type: Boolean,
            value: false
        },
        usersession: {
          type: Object
        }
    };
  }

  ready() {
    super.ready();
    this.addEventListener('conexionOk', this.conexionOk);
    this.addEventListener('cierraSession', this.cierraSession);
    console.log(sessionStorage.getItem("user-bbva-session"));
    if(sessionStorage.getItem("user-bbva-session")){
      this.authenticated = true;
      this.usersession = JSON.parse(sessionStorage.getItem("user-bbva-session"));
    }

    
  }

  conexionOk(event){
    this.authenticated = true;
    console.log(event.detail);
    sessionStorage.setItem("user-bbva-session", event.detail);
    //this.usersession = "OK";
    this.usersession = JSON.parse(sessionStorage.getItem("user-bbva-session"));

    /*
    this.dispatchEvent(new CustomEvent('actualizarCuentas', {bubbles: true, composed: true,
      detail: ""}));*/

      console.log("luego de invocar a actualizar cuentas");
  }

  cierraSession(event){
    this.authenticated = false;
    sessionStorage.removeItem("user-bbva-session");
    this.usersession = {};
  }
/*
  cargarDatos(){
    var session = sessionStorage.getItem("user-bbva-session");

  }*/

  /*
  initializeDefaultUserSession(){
    usersession
  }*/
}

window.customElements.define('bbvamain-app', BBVAMainApp);
